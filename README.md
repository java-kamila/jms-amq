# ActiveMQ e JMS

 - MOM (Message Oriented Middleware): guarda a mensagem recebida e algum momento depois a entregará para outro sistema, de forma assíncrona.

 - AMQ é o mais famoso MOM no mundo Java, da Apache Foundation.

### JMS e o padrão Observer*
O JMS segue o mesmo padrão de projeto [*Observer*](http://www.enterpriseintegrationpatterns.com/patterns/messaging/ObserverJmsExample.html)! A diferença é que JMS é remoto ou distribuído. Ou seja, no padrão Observer originalmente descrito no livro GOF, tudo acontece na memória, o Observer desacopla objetos. Com JMS, a comunicação entre Producer e Consumer é remota, desacoplamento arquitetural.

### Instalação 
 - [Download](http://activemq.apache.org/download.html)
 - OSX: na pasta `apache-activemq-5.12.2/bin` executar o comando `sh activemq console`
 - Windows: na pasta `apache-activemq-5.12.2/bin` executar o comando `activemq start`. Ou entre na pasta "...\apache-activemq-5.12.0\bin\win64", execute o arquivo "activemq.bat". 
 No Windows é preciso executar o script InstallService.bat da pasta win32 ou win64 dependendo da arquitetura do computador.
 - Endereço do servidor: `http://localhost:8161` ou `http://localhost:8161/admin`,  login: "admin" e senha "admin".

### Simulando a entrega da mensagem
Execute os comandos na pasta que contem o *jar* do AMQ e do curso:
 - No Linux e Mac:
 
 `java –cp activemq-all-5.12.0.jar:aula-jms.jar br.com.caelum.TesteMensageria consome`
 - No Windows:
 `java –cp activemq-all-5.12.0.jar;aula-jms.jar br.com.caelum.T`

### Consumidor JAVA

```java
package br.com.alura.jms;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.naming.InitialContext;

public class TesteConsumidor {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		InitialContext context = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory"); // Nome q registra ConnectionFactory
		Connection connection = factory.createConnection();
		connection.start();
		
		// Para execução na próxima linha para evitar fechar conexão
		new Scanner(System.in).nextLine();
		
		connection.close();
		context.close();
		
	}
	
}

```

##### InitialContext
 - `InitialContext` altomaticamente busca configurações *jndi* no seu constructor ou no arquivo jndi.properties, dentro da pasta src.
 - O lookup para `ConnectionFactory` é feito através da classe `InitialContext` que por sua vez se baseia no arquivo de configuração jndi.properties. Padrão **JNDI** (Java Naming e Diretory Service).
 - Exemplo de arquivo *jndi* em: [https://activemq.apache.org/jndi-support]
 - O ActiveMQ não é um servidor de aplicação e sim um Message Broker, ele disponibiliza a ConnectionFactory.
 - A propriedade `java.naming.provider.url` recebe o IP do MOM que queremos enviar/consumir mensagens. Além disso, essa propriedade faz parte do arquivo `jndi.properties`.

##### Properties
 - Em Java temos uma classe que representa arquivos de propriedades: a classe `Properties`. E podemos usá-la para configurar o contexto do JNDI no lugar do arquivo `jndi.properties`.
 
```java
		Properties properties = new Properties();
		properties.setProperty("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		properties.setProperty("java.naming.provider.url", "tcp://192.168.1.70:61616");
		properties.setProperty("queue.financeiro", "fila.financeiro");
		
		InitialContext context = new InitialContext(properties);
```

##### Session
 - A `Session` no JMS abstrai o trabalho transacional e confirmação do recebimento da mensagem. Além disso, também serve para produzir o `MessageConsumer`.
 - O objeto Session do JMS `(javax.jms.Session)` é o objeto responsável pela criação de diversos componentes do JMS como *Producer*, *Consumer*, *Browser* e implementações de *Message*.
 - O primeiro parâmetro do método `createSession` define se queremos usar o tratamento da transação como explícito. Como colocamos false, não é preciso chamar `session.commit()` ou `session.rollback()`
 - O `Session.AUTO_ACKNOWLEDGE` diz que queremos automaticamente (através da Session) confirmar o recebimento da mensagem JMS.
 
##### Destination

 - O ActiveMQ ou MOM em geral pode ter vários consumidores e receber mensagens de vários clientes. Para organizar o recebimento e a entrega das mensagens criamos destinos (ou Destination) no MOM. A nossa fila.financeiro é um Destination, ou seja, o lugar concreto onde a mensagem será salvo temporariamente.

 - No mundo JMS um destination é representado pela interface `javax.jms.Destinaton` e fizemos um lookup para pegá-lo:

`Destination fila = (Destination) context.lookup("fila.financeiro");`

 - Segue também o JavaDoc da interface Java que representa um destino: [http://docs.oracle.com/javaee/5/api/javax/jms/Destination.html]

##### JMS 1.1 e 2.0
 - Passos para criar um `MessageConsumer` com JMS 1.1:

`ConnectionFactory -> Connection  -> Session -> MessageConsumer`
 - No JMS 2.0 o desenho seria um pouco mais simples pois foi introduzido uma nova interface `JMSContext` que combina o trabalho da Connection e Session:

`ConnectionFactory -> JMSContext -> JMSConsumer`

- Conexão estabelecida

 <img src="conexao_consumidor.png">
 
##### Importando documentação de MessageListener

 - Fazer download da documentação em [http://s3.amazonaws.com/caelum-online-public/jms/jms-apidocs.zip]
 - Extrair arquivos e inserir pasta `jms1.1` na pasta do projeto.
 - Ao clicar em "Open declaration" de `MessageListener`, selecionar Atach e selecionar a pasta `jms1.1/src/share`.

##### MessageListener
Inicialmente o sistema recebia apenas uma mensagem e encerrava o seu funcionamento, para que ele fique escutando o tempo todo por mensagens vamos fazer com o que Consumer delegue o tratamento das mensagens para um objeto da interface MessageListener:

`consumer.setMessageListener();`

### Produtor JMS

```java
public class TesteProdutor {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
	
		IntialContext context = new InitialContext();
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
		
		Connection connection = factory.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Destination fila = (Destination) context.lookup("financeiro");
		MessageProducer producer = session.createProducer(fila);
		
		Message message = session.createTextMessage("<pedido><id>123</id></pedido>");
		producer.send(message);
		
		session.close();
		connection.close();
		context.close;
}
```

### Concorrência / Competição entre consumidores

Quando trabalhamos com filas, as mensagens são entregues apenas para um consumidor. Se houver mais de um consumidor online a mensagem é entregue apenas para um dos (e nunca para os dois). Havendo uma distribuição das entregas, um balanceamento de carga.

 - Imagem representando comunicação distribuída:
 <img src="amq_concorrencia.png">
 
 - A ConnectionFactory e a fila (Destination) pegamos através do lookup usando o InitialContext. Representação da construção do Producer e Consumer:
  <img src="resumo-producer-consumer.png">
  
### QueueBrowser
 - Podemos precisar apenas checar (monitoramento) as mensagens que chegaram para uma determinada fila sem consumi-la. Ou seja, apenas queremos ver sem tirá-las da fila. Para isso podemos usar um componente do JMS chamado [QueueBrowser](https://docs.oracle.com/javaee/7/api/javax/jms/QueueBrowser.html), usado para navegar sobre as mensagens sem consumi-las.
 
 ```java
 	Destination fila = (Destination) context.lookup("financeiro");        
	QueueBrowser browser = session.createBrowser((Queue) fila);
	
	// Visualizando mensagens
	Enumeration msgs = browser.getEnumeration();
	while (msgs.hasMoreElements()) { 
    TextMessage msg = (TextMessage) msgs.nextElement(); 
    System.out.println("Message: " + msg.getText()); 
}

 ```

### Topics

 - Filas enviam uma mensagem apenas para um consumidor, já os Tópicos enviam uma mesma mensagem para diversos consumidores simultaneamente.
 - Para a entrega da mensagem ao tópico é preciso que o consumidor se identifique.
 - Se tivermos consumidores identificados (assinaturas duráveis) o tópico vai guardar a mensagem até a entrega acontecer, até o consumidor estar online novamente.
 - Para que as mensagens que o Tópico entrega não sejam perdidas quando os Consumidores estão offline devemos identificar uma conexão com o método:

`connection.setClientID("comercial");`

E identificar o consumidor com o método:

`MessageConsumer consumer = session.createDurableSubscriber(topico, "assinatura");`

- **Assinaturas duráveis** só existem para tópicos. Uma assinatura durável é nada mais do que um consumidor de um tópico que se identificou. Ou seja, o tópico sabe da existência desse consumidor.
- O tópico, por padrão, não garante a entrega da mensagem, pois não sabe se existe 1 ou 20 consumidores. Então de cara, o tópico só entrega as mensagens para consumidores que estiverem online

<img src="topicos.png">

### Modelos Tópico e Fila
 - Queue: Sender, Receive.
 - Topic: Publisher, Subscriber

<img src="modelos_fila_topico.png" />

### Selector

`MessageConsumer consumer = session.createDurableSubscriber(topico, "assinatura", "ebook=true", false);`

 - Usamos selectors (seletores) para que consumers processem determinadas mensagens e não todas.
 - Não podemos usar messages selectors para buscar algum valor dentro do corpo da mensagem, porém ele permite buscar dentro do cabeçalho e propriedades da mensagem.
 - O segundo ponto é que os messages selectors possuem uma sintaxe parecida com SQL para busca de informações.
 - Message selectors são armazenados no header da mensagem e nunca no corpo. Podemos ter regras mais complexas, adicionando no terceiro parâmetro o operador lógico OR.
 - Uma desvantagem seria que a regra/condição do recebimento da mensagem está nos consumidores. Muitas vezes queremos centralizar essas regras no lado do servidor.
