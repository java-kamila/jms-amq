package br.com.alura.jms;

import java.util.Properties;
import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

public class TesteProdutorFila {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		// Configurando contecto do JNDI no lugar do arquivo jndi.properties
		Properties properties = new Properties();
		properties.setProperty("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		properties.setProperty("java.naming.provider.url", "tcp://192.168.1.70:61616");
		properties.setProperty("queue.financeiro", "fila.financeiro");
		
		InitialContext context = new InitialContext(properties);
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory"); // Nome q registra ConnectionFactory
		Connection connection = factory.createConnection();
		// Conecta com servidor MOM
		connection.start();

		/*
		 *  Configura transações e confirmação de recebimento de mensagens
		 *  1° parâmetro: false, indica que não iremos realizar uma transação
		 *  2° parâmetro: Session.AUTO_ACKNOWLEDGE indica que altomaticamente irá confirmar o recebimento da mensagem
		 */	  
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		
		// Nome da queue estabelecido em jndi.properties: "queue.financeiro"
		Destination fila = (Destination) context.lookup("financeiro");
		
		session.createProducer(fila);
		
		MessageProducer producer = session.createProducer(fila);
		
		for (int i = 0; i < 100; i++) {
			Message message = session.createTextMessage("<pedido><id>" + i + "</id></pedido>");
			producer.send(message);			
		}
		
		// Para execução na próxima linha para evitar fechar conexão
		new Scanner(System.in).nextLine();
		
		// Fechando recursos
		session.close();
		connection.close();
		context.close();
		
	}
	
}
