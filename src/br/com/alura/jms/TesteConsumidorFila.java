package br.com.alura.jms;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

public class TesteConsumidorFila {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		InitialContext context = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory"); // Nome q registra ConnectionFactory
		Connection connection = factory.createConnection();
		// Conecta com servidor MOM
		connection.start();

		/*
		 *  Configura transações e confirmação de recebimento de mensagens
		 *  1° parâmetro: false, indica que não iremos realizar uma transação
		 *  2° parâmetro: Session.AUTO_ACKNOWLEDGE indica que altomaticamente irá confirmar o recebimento da mensagem
		 */	  
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		
		// Nome da queue estabelecido em jndi.properties: "queue.financeiro"
		Destination fila = (Destination) context.lookup("financeiro");
		MessageConsumer messageConsumer = session.createConsumer(fila);

		messageConsumer.setMessageListener(new MessageListener() {
			
			@Override
			public void onMessage(Message message) {
				
				TextMessage textMessage = (TextMessage)message;
				
				try {
					System.out.println("Consumidor Fila Financeiro: " + textMessage.getText());
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
			
		});
		
		
		// Para execução na próxima linha para evitar fechar conexão
		new Scanner(System.in).nextLine();
		
		session.close();
		connection.close();
		context.close();
		
	}
	
}
