package br.com.alura.jms;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

public class TesteProdutorTopico {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
				
		InitialContext context = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory"); // Nome q registra ConnectionFactory
		Connection connection = factory.createConnection();
		// Conecta com servidor MOM
		connection.start();

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		// Nome da queue estabelecido em jndi.properties: "queue.financeiro"
		Destination topico = (Destination) context.lookup("loja");
		
		session.createProducer(topico);
		
		MessageProducer producer = session.createProducer(topico);
		
		Message message = session.createTextMessage("Topico <pedido><id>102</id><e-book>false</e-book></pedido>");
		message.setBooleanProperty("ebook", false);
		producer.send(message);			
				
		// Para execução na próxima linha para evitar fechar conexão
		new Scanner(System.in).nextLine();
		
		// Fechando recursos
		session.close();
		connection.close();
		context.close();
		
	}
	
}
